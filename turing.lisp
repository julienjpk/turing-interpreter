;;; ----------------------------------------------------------------------------
;;; This program is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by the Free
;;; Software Foundation, either version 3 of the License, or (at your option)
;;; any later version.
;;; 
;;; This program is distributed in the hope that it will be useful, but WITHOUT
;;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;;; FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
;;; more details.
;;; 
;;; You should have received a copy of the GNU General Public License along with
;;; this program. If not, see <http://www.gnu.org/licenses/>.
;;; ----------------------------------------------------------------------------

;;;; CHECK-WORD
;;; Checks a word's validity by looking up its letters into a reference
;;; alphabet. Will return t if the word is valid, nil otherwise.

(defun check-word (word alphabet)
  ;; Unwrapping the word, building a (and) sequence over its letters. If all
  ;; letters match, the sequence will evaluate to t, nil otherwise.
  (cond ( (eq (length word) 0) t)
	( t (and (> (count (char word 0) alphabet) 0)
		 (check-word (subseq word 1) alphabet))))
  )

;;;; CHECK-TRANSITIONS-SET
;;; Checks the validity of a transition set.

(defun check-transitions-set (trs)
  (cond ((null trs) t)
	;; Check the first element. If there's a single problem, return nil.
	((or (not (eq 5 (list-length (car trs))))
	     (not (eq 2 (+ (length (nth 1 (car trs)))
			   (length (nth 3 (car trs))))))
	     (null (member (nth 4 (car trs)) '(L R)))) nil)
	;; Check the rest of the transitions.
	(t (check-transitions-set (cdr trs))))
  )

;;;; CLEAR-VALIDATE
;;; Checks whether state is a final/accepting state. Appends "V" to the tape if
;;; it is, "X" otherwise. Also clears the tape of the unecessary blank symbols.

(defun clear-and-validate (tape state final blank)
  (cond ((null final)
	 (concatenate 'string "X|" (string-trim blank tape)))
	((eq state (car final))
	 (concatenate 'string "V|" (string-trim blank tape)))
	(t (clear-and-validate tape state (cdr final) blank)))
  )

;;;; TRACE-LOG
;;; Prints the given transition's description and returns it. This helps
;;; creating a program trace without too much trouble.

(defun trace-log (tape tr final blank)
  (format t "[~A] (~a,~a -> ~a,~a,~a)~C~C"
	  (clear-and-validate tape (nth 2 tr) final blank)
	  (nth 0 tr) (nth 1 tr) (nth 2 tr) (nth 3 tr) (nth 4 tr)
	  #\return #\newline)
  tr
  )

;;;; FIND-TRANSITION
;;; Searches trs for a transition starting in state and ending in tape[head].

(defun find-transition (head tape state trs final blank)
  (cond ((null trs) nil )
	;; Check the head transition. If that's the one, return and log it.
	((and (eq (car (car trs)) state)
	      (string= (nth 1 (car trs)) (char tape head))
	      (not (and (eq head 0) (eq 'L (nth 4 (car trs))))))
	 (trace-log tape (car trs) final blank))
	;; Not a match: trying the next one.
	( t (find-transition head tape state (cdr trs) final blank)))
  )

;;;; READ-WORD
;;; Basically gets the tape through the machine, and returns the final result.

(defun read-word (head tape state final trs blank)
  (declare (special *t* *x*))

  ;; Tape overflow: if the head is set after the end of the word, add a blank
  ;; symbol.
  (if (>= head (length tape)) (setq tape (concatenate 'string tape blank)))
  
  ;; Looking for the next transition... We'll (setq) it so we don't have to look
  ;; it up too many times.
  (setq *t* (find-transition head tape state trs final blank))
  
  ;; Transforming the transition's final element into -1 (L), 0 (S) or +1 (R).
  ;; This will allow us to increment/decrement head for the next call.
  (setq *x* (if (eq 'L (nth 4 *t*)) -1 1))
  
  ;; If no transition was found, the machine's done: return the tape. Otherwise,
  ;; call READ-WORD again after shifting the head and editing the tape.
  (cond ((null *t*) (clear-and-validate tape state final blank))
	(t (read-word (+ head *x*)
		      (replace tape (nth 3 *t*) :start1 head :end1 (+ head 1))
		      (nth 2 *t*) final trs blank)))
  )

;;;; COMPUTE-MACHINE
;;; Prepares the program's arguments and makes the first call to READ-WORD, with
;;; the head on position 0.

(defun compute-machine (machine tape)
  (declare (special *blank*))

  ;; Checking the description...
  (cond ((not (eq (list-length machine) 5))
	 (error "The machine's representation should have 5 elements."))

	((eq (* (length (nth 0 machine)) (length (nth 1 machine))) 0)
	 (error "None of the alphabets may be empty."))

	((> (count (char (nth 0 machine) 0) (nth 1 machine)) 0)
	 (error "The blank symbol may not be in the tape alphabet."))

	((not (check-transitions-set (nth 4 machine)))
	 (error "The transitions set is invalid."))

	((not (check-word (nth 1 machine) (nth 0 machine)))
	 (error "The tape alphabet must be a subset of the input alphabet."))

	((not (check-word tape (nth 1 machine)))
	 (error "The tape is incompatible with the machine's alphabets.")))
  
  ;; First call the READ-WORD, in position 0. This function will return the
  ;; final tape appending with a V or an X, depending on whether the final state
  ;; is an accepting one.
  (setq *blank* (subseq (car machine) 0 1))
  (format t "[~A]" (read-word 0 tape (nth 2 machine) (nth 3 machine)
			      (nth 4 machine) *blank*))
  )

;;;; Entry point.
;;; Just making sure we have two arguments, and reading the files. I'll let the
;;; interpreter be angry if there's any file access problem.
(if (< (list-length *args*) 2)
    (format t "Usage: clisp turing [machine.tur] [tape]")
    (compute-machine (read (open (nth 0 *args*))) (nth 1 *args*)))
