# Turing Machine Interpreter

[![License](https://img.shields.io/badge/license-GPL-blue.svg)](https://raw.githubusercontent.com/julienjpk/turing-interpreter/master/LICENSE)

This is a rather simple Turing machine interpreter written in CLisP. Given a
machine description file, and an initial tape, it will execute the machine until
it reaches a blocking configuration (assuming it will). The program will output
its transition trace, its final tape and whether or not the machine ends up in
an accepting state.

For instance, given the `odd.tur` example machine (accepts odd numbers), and the
`0000` input (number 4) :

    $ clisp turing machines/odd.tur "0000"
    [V|0000] (0,0 -> 1,0,R)
    [X|0000] (1,0 -> 0,0,R)
    [V|0000] (0,0 -> 1,0,R)
    [X|0000] (1,0 -> 0,0,R)
    [X|0000]

              ^^^^^^^^^^^^ last transition followed
       ^^^^ tape symbols
     ^ V when reaching an accepting state, X otherwise.

Machine descriptions (`.tur` files) are rather simple, and mimic LisP lists.
Here's the previous `odd.tur` machine:

    ( "#0" "0" 0 (1) (
      (0 "0" 1 "0" R)
      (1 "0" 0 "0" R) ))

- The first element, `"#0"`, is the tape alphabet with the blank symbol as its
first element. This should be a quoted string.
- The second element, `"0"`, is the input alphabet. This should be a subset of
the previous alphabet.
- The third element, `0` is the initial state, a number.
- The fourth element, `(1)` are the accepting states. For instance: `(0 1 2 3)`.
- The fifth element is the transition function's description: a set of
transition settings.

Each transition in the final list should have 5 elements (`p,a -> q,b,x`). In
order:

- The initial state from which the transition may be used.
- The required tape symbol under the head for this transition to be usable.
- The destination state once the previous conditions are met.
- The output symbol replacing the current symbol under the head.
- The shifting direction, either L (left) or R (right).

This code is released under the GPLv3 License.
